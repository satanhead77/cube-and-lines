﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowingCameraController : BaseController
{
	[SerializeField] private float _damping = 1.5f;
	[SerializeField] private Vector3 _offset;
	[SerializeField] private string _targetTag;
	private Transform _target;
	private int _lastZ;

	void Start()
	{
		_offset = new Vector3(_offset.x, _offset.y, _offset.z);
		FindPlayer();
	}

	public void FindPlayer()
	{
		_target = GameObject.FindGameObjectWithTag(_targetTag).transform;
		_lastZ = Mathf.RoundToInt(_target.position.z);

		Position = new Vector3(_target.position.x, _target.position.y + _offset.y, Position.z + _offset.z);
	}

	public override void ControllerUpdate()
	{
		if (_controllerIsEnabled)
		{
			CameraFolow();
		}
	}

	private void CameraFolow()
	{
		int currentZ = Mathf.RoundToInt(_target.position.z);
		_lastZ = Mathf.RoundToInt(_target.position.z);

		Vector3 target;

		target = new Vector3(_target.position.x, _target.position.y + _offset.y, _target.position.z - _offset.z);

		Vector3 currentPosition = Vector3.Lerp(Position, target, _damping * Time.deltaTime);
		Position = currentPosition;
	} 
}
