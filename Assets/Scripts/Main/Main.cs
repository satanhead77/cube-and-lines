﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{
	#region fields

	[SerializeField] private BaseObjectScene _player;
	[SerializeField] private GameRestartController _restartController;
	[SerializeField] private FollowingCameraController _cameraController;
	[SerializeField] private BaseInput _input;
	[SerializeField] private PlayerMaterials _playerMaterials;
	[SerializeField] private TrapsController _trapsController;
	[SerializeField] private PlayerStatistics _playerStatistics;
	[SerializeField] private AdsController _adsController;


	private MoveController _moveController;
	private PlayerRotationController _rotationController;
	private CubeLineRenderController _cubeLineRenderController;
	private PlayerSpeedController _playerSpeedController;
	private LevelEnd _levelEnd;
	 


	private JsonSaver _jsonSaver;
	private GameObject _playerGameObject;


	private List<BaseController> controllersList = new List<BaseController>();

	#endregion


	#region properties

	public static Main Instance { get; private set; }
	public BaseObjectScene GetPlayer { get { return _player; } }
	public BaseInput Input { get { return _input; } }
	public PlayerMaterials PlayerMaterials { get { return _playerMaterials; } }
	public PlayerSpeedController PlayerSpeedController { get { return _playerSpeedController; } }
	public GameRestartController RestartController { get { return _restartController; } }

	public LevelEnd LevelEnd { get { return _levelEnd; } }
	public PlayerStatistics PlayerStatistics { get { return _playerStatistics; } }
	public AdsController AdsController { get { return _adsController; } }

	public JsonSaver JsonSaver { get { return _jsonSaver; } }

	 
	#endregion

	#region Methods

	void Awake()
    {
		_playerGameObject =_player.gameObject;

		Instance = this;

		_jsonSaver = new JsonSaver("SaveValues", _playerStatistics, _playerMaterials);
		_jsonSaver.LoadGame();

		_playerSpeedController = _playerGameObject.GetComponent<PlayerSpeedController>();
		_moveController = _playerGameObject.GetComponent<MoveController>();
		_rotationController = _playerGameObject.GetComponent<PlayerRotationController>();
		_cubeLineRenderController = _playerGameObject.GetComponent<CubeLineRenderController>();

		controllersList.Add(_playerSpeedController);
		controllersList.Add(_moveController);
		controllersList.Add(_rotationController);
		controllersList.Add(_cubeLineRenderController);
		controllersList.Add(_restartController);
		controllersList.Add(_cameraController);
		controllersList.Add(_trapsController);

		_levelEnd = _playerGameObject.GetComponent<LevelEnd>();

		SubscribeToEvents();

	}

	private void OnDestroy()
	{
		UnSubscribeFromEvents();
	}

	void Update()
	{
		foreach (var controller in controllersList)
		{
			controller.ControllerUpdate();
		}
	}

	#endregion

	#region events

	private void SubscribeToEvents()
	{
		_restartController.GameRestarted += _moveController.RestartWay;
		//_playerStatistics.NumberOfScoresChangeEvent += UiManager.Instance.GameplayCanvas.UpdateScoresCountText /*_gamePlayCanvas.UpdateScoresCountText*/;
	}

	private void UnSubscribeFromEvents()
	{
		_restartController.GameRestarted -= _moveController.RestartWay;
		//_playerStatistics.NumberOfScoresChangeEvent -= _gamePlayCanvas.UpdateScoresCountText;
	}

	#endregion
}
