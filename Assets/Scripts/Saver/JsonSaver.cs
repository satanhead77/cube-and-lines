﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;

public class JsonSaver 
{
	private string _saveValuesFileName;
	//private Score _score;
	PlayerStatistics _playerStatistics;
	private PlayerMaterials _playerMaterials;
	 

	public JsonSaver(string saveValuesFileName, PlayerStatistics playerStatistics, PlayerMaterials playerMaterials)
	{
		_saveValuesFileName = saveValuesFileName;
		_playerStatistics = playerStatistics;
		_playerMaterials = playerMaterials;
	}

	private string MakePath(string name)
	{
		string path;

  //      #if UNITY_EDITOR
		//path = Path.Combine(Application.streamingAssetsPath.Replace("StreamingAssets", "Saves"), name);
  //      #endif
		path = Path.Combine(Application.persistentDataPath.Replace("StreamingAssets", "Saves"), name);

		return path;
	}

	public void SaveGame()
	{
		SavePlayerStatsValuesAndCurrentPlayerSkin(_playerStatistics.scoresCount, _playerMaterials._currentSkinIndex,_playerStatistics.deathsCount,_playerStatistics.numberOfPassedLevels);
	}

	private void SavePlayerStatsValuesAndCurrentPlayerSkin(int scoresCount, int currentPlayerSkin, int deathCount, int numberOfPassedLevels)
	{
		File.WriteAllText(MakePath(_saveValuesFileName), JsonConvert.SerializeObject(new GameSaveInfo(scoresCount, currentPlayerSkin, deathCount, numberOfPassedLevels)));
	}

	public void SaveSkinState(string fileName, bool skinState)
	{
		File.WriteAllText(MakePath(fileName), JsonConvert.SerializeObject(new SkinStateInfo(skinState)));
	}

	public void LoadGame()
	{
		if (File.Exists(MakePath(_saveValuesFileName)))
		{
			using (var file = File.Open(MakePath(_saveValuesFileName), FileMode.Open))

			using (var streamReader = new StreamReader(file))
			{
				var json = streamReader.ReadToEnd();

				var Info = JsonConvert.DeserializeObject<GameSaveInfo>(json);
				_playerStatistics.scoresCount = Info.scoresCount;
				_playerMaterials._currentSkinIndex = Info.currentPlayerSkin;
				_playerStatistics.deathsCount = Info.deathCount;
				_playerStatistics.numberOfPassedLevels = Info.numberOfPassedLevels;
			}
		}
	}

	public bool LoadSkinsState(string fileName)
	{
		bool skinState = false;

		if (File.Exists(MakePath(fileName)))
		{
			using (var file = File.Open(MakePath(fileName), FileMode.Open))

			using (var streamReader = new StreamReader(file))
			{
				var json = streamReader.ReadToEnd();

				var Info = JsonConvert.DeserializeObject<SkinStateInfo>(json);

				skinState = Info.skinIsOpen;
			}
		}

		return skinState;
	}
}
