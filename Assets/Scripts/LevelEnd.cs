﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelEnd : MonoBehaviour
{
	public event Action LevelFinished;

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Chest")
		{
			LevelFinished?.Invoke();
		}
	}
}
