﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCompleteCanvas : BaseUi
{
	[SerializeField] private SceneChanger _sceneChanger;

    public void NextLevelButtonClick()
	{
		_sceneChanger.ChangeScene();
	}

	public void LevelFinishedHandler()
	{
		EnableCanvas();
	}
}
