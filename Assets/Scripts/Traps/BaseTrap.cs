﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class BaseTrap : BaseObjectScene
{
	[SerializeField] protected string _enemyTag;
	[SerializeField] protected GameObject effect;
	[SerializeField] protected int _effectsNumber;


	public abstract void TrapMove();

	#region EnemyDisableMethods

	protected void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == _enemyTag)
		{
			DisableEnemyWithEffects();
			Debug.Log("Trap");
		}
	}

	protected void DisableEnemyWithEffects()
	{
		CreateEffects();
		Main.Instance.GetPlayer.InstanceObject.SetActive(false);
		Main.Instance.RestartController.PlayerDisableHandler();
		UiManager.Instance.RestartCanvas.PlayerDeathHandler();
		Main.Instance.PlayerStatistics.AddDeath();
	}

	protected void CreateEffects()
	{
		for (int i = 0; i < _effectsNumber; ++i)
		{
			var effectObj = Instantiate(effect, Main.Instance.GetPlayer.Position, Quaternion.identity);

			var effectMesh = effectObj.GetComponent<MeshRenderer>();

			effectMesh.material = Main.Instance.GetPlayer.MyMeshRenderer.material;
		}
	}

	#endregion
}
