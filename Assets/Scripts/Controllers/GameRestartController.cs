﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRestartController : BaseController
{

	public event Action GameRestarted;
	[SerializeField] private bool _didThePlayerDie;
	[SerializeField] private Vector3 _playerStartPosition;
	private float _timer = 0.5f;

	void Start()
	{
		_playerStartPosition = Main.Instance.GetPlayer.transform.position;
	}

	public override void ControllerUpdate()
	{
		if(_controllerIsEnabled)
		RestartGame();
	}

	private void RestartGame()
	{
		if (_didThePlayerDie && TimerIsExpired())
		{
			if (Main.Instance.Input.Input())
			{
				PlayerRespewn();
				CollectAndDestroyPlayersFragments();
			}
		}
	}

	private void PlayerRespewn()
	{
		GameRestarted?.Invoke();
		Main.Instance.GetPlayer.InstanceObject.SetActive(true);
		_didThePlayerDie = false;
		Main.Instance.GetPlayer.transform.position = _playerStartPosition;
	}

	private bool TimerIsExpired()
	{
		_timer -= Time.deltaTime;

		if (_timer <= 0)
		{
			_timer = 0.5f;

			return true;
		}

		return false;
	}

	private void CollectAndDestroyPlayersFragments()
	{
		var cubeFragments = GameObject.FindGameObjectsWithTag("CubeFragment");

		foreach (var cubeFragment in cubeFragments)
		{
			Destroy(cubeFragment);
		}
	}

	public void PlayerDisableHandler()
	{
		_didThePlayerDie = true;
	}

}
