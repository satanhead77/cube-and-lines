﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeLineRenderController : BaseController
{

	[SerializeField] private Transform _playerLinePos;
	[SerializeField] private GameObject _linePrefab;

	private GameObject _currentLine;
	private LineRenderer lineRenderer;
	private List<Vector3> _playerPositions;
	private List<GameObject> _createdLinesList;

	private void Start()
	{
		_playerPositions = new List<Vector3>();
		_createdLinesList = new List<GameObject>();
	}

	 
	private void OnDisable()
	{
		foreach (var line in _createdLinesList)
		{
			Destroy(line);
		}

		_createdLinesList.Clear();
		_playerPositions.Clear();
	}

	public override void ControllerUpdate()
	{
		if (_controllerIsEnabled)
		{
			if (gameObject.activeSelf)
			{
				if (Input.GetMouseButtonDown(0))
				{
					CreateLine();
				}
				if (Input.GetMouseButton(0))
				{
					Vector3 tempPlayerPos = _playerLinePos.position;

					if (Vector3.Distance(tempPlayerPos, _playerPositions[_playerPositions.Count - 1]) > .1f)
					{
						UpdateLine(tempPlayerPos);
					}
				}
			}
		}
	}

	private void CreateLine()
	{
		_currentLine = Instantiate(_linePrefab, Vector3.zero, Quaternion.identity);

		_createdLinesList.Add(_currentLine);

		lineRenderer = _currentLine.GetComponent<LineRenderer>();
		SetLineMaterial();
		_playerPositions.Clear();

		_playerPositions.Add(_playerLinePos.position);
		_playerPositions.Add(_playerLinePos.position);

		lineRenderer.SetPosition(0, _playerPositions[0]);
		lineRenderer.SetPosition(1, _playerPositions[1]);
	}

	private void UpdateLine(Vector3 newPlayerPos)
	{
		_playerPositions.Add(newPlayerPos);
		lineRenderer.positionCount++;
		lineRenderer.SetPosition(lineRenderer.positionCount - 1, newPlayerPos);
	}

	private void SetLineMaterial()
	{
		lineRenderer.material = Main.Instance.PlayerMaterials.currentMaterial;
	}


	 

}
