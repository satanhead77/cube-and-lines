﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerStatistics : MonoBehaviour
{
   public int deathsCount;
   public int numberOfPassedLevels;
   public int scoresCount;
   [SerializeField] private int _scoresForFinishLevel;// сколько очков игрок получает за конец уровня 
   public event Action<int> NumberOfScoresChangeEvent;
   public event Action AddDeathEvent;

	private void Start()
	{
		Main.Instance.LevelEnd.LevelFinished += LevelEndHandler;
		NumberOfScoresChangeEvent.Invoke(scoresCount);
	}

	private void OnDestroy()
	{
		Main.Instance.LevelEnd.LevelFinished -= LevelEndHandler;
	}

	public void AddDeath()
	{
		deathsCount++;
		AddDeathEvent.Invoke();
		Main.Instance.JsonSaver.SaveGame();
	}

	public void AddPassedLevel()
	{
		numberOfPassedLevels++;
		Main.Instance.JsonSaver.SaveGame();
	}

	public void AddScores(int addedScores)
	{
		scoresCount += addedScores;
		Main.Instance.JsonSaver.SaveGame();
		NumberOfScoresChangeEvent?.Invoke(scoresCount);
	}

	public bool RemoveScores(int removedScores)
	{
		if (scoresCount >= removedScores)
		{
			scoresCount -= removedScores;
			Main.Instance.JsonSaver.SaveGame();
			NumberOfScoresChangeEvent?.Invoke(scoresCount);
			return true;
		}

		return false;
	}

	private void LevelEndHandler()
	{
		AddScores(_scoresForFinishLevel);
		AddPassedLevel();
	}
}
