﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsController : MonoBehaviour
{
	[SerializeField] private bool _isTestMode;
	[SerializeField] private string _gameId = "3921145";
	[SerializeField] private bool _isAdsEnabled = true;
	[SerializeField] private int _needDeathToShowVideo;
	[SerializeField] private int _deathCount;
	 

	private string _videoWithTheOptionToSkip = "video";

	private string _videoWithoutTheOptionToSkip = "rewardedVideo";

	void Start()
	{
		Init();
		Main.Instance.PlayerStatistics.AddDeathEvent += ShowVideoAfterDeaths;
	}

	private void OnDestroy()
	{
		Main.Instance.PlayerStatistics.AddDeathEvent -= ShowVideoAfterDeaths;
	}

	private void Init()
	{
		Advertisement.Initialize(_gameId, _isTestMode);
	}

	private void ShowVideo(string tipeOfvideo)
	{
		if (_isAdsEnabled)
		{
			if (Advertisement.IsReady())
			{
				Advertisement.Show(tipeOfvideo);
			}
		}
	}

	private void ShowVideoAfterDeaths()
	{
		if (_deathCount == _needDeathToShowVideo)
		{
			ShowVideo(_videoWithoutTheOptionToSkip);
			_deathCount = 0;
		}
		else
		{
			_deathCount++;
		}
	}

	public void ShowRewardedVideo()
	{
		ShowVideo(_videoWithoutTheOptionToSkip);
	}
}
