﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PcInput : BaseInput
{
	[SerializeField] private KeyCode _inputButton;

	public override bool Input()
	{
		if (UnityEngine.Input.GetKey(_inputButton))
		{
			return true;
		}

		return false;
	}
}
