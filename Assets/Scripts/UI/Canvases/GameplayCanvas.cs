﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameplayCanvas : BaseUi
{
	[SerializeField] private bool _optionsButtonCliced;

	[SerializeField] private List<GameObject> _buttonsList;

	[SerializeField] private TextMeshProUGUI _scoresCountText;

	public void OptionsButtonClicK()
	{
		if (!_optionsButtonCliced)
		{
			_optionsButtonCliced = true;

			ButtonsSetActive(true);
		}
		else
		{
			ButtonsSetActive(false);
			_optionsButtonCliced = false;
		}
	}

	private void ButtonsSetActive(bool isButtonsActive)
	{
		foreach (var button in _buttonsList)
		{
			button.SetActive(isButtonsActive);
		}
	}

	public void UpdateScoresCountText(int scoresCount)
	{
		_scoresCountText.text = scoresCount.ToString();
	}
}
