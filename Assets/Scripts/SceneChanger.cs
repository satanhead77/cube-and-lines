﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{

	public void ChangeScene()
	{
		SceneManager.LoadScene(NextScene());
	}

	private int NextScene()
	{
		int nextSceneIndex = 0;

		nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;

		if(nextSceneIndex > SceneManager.sceneCount)
		{
			nextSceneIndex = 0;
		}

		return nextSceneIndex;
	}

}
