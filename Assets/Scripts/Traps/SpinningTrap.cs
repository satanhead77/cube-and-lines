﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinningTrap : BaseTrap
{
	[SerializeField] private float _rotationSpeed;
	[SerializeField] private Transform _rotateTarget;

	public override void TrapMove()
	{
		Rotate();
	}

	private void Rotate()
	{
		Rotation = Quaternion.RotateTowards(Rotation, _rotateTarget.rotation, _rotationSpeed * Time.deltaTime);
	}
}
