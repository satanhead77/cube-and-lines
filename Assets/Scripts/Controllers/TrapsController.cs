﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapsController : BaseController
{
	[SerializeField] private List<BaseTrap> _trapsList;

	public override void ControllerUpdate()
	{
		if (_controllerIsEnabled)
		{
			foreach (var trap in _trapsList)
			{
				trap.TrapMove();
			}
		}
	}
}
