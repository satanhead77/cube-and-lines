﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartCanvas : BaseUi
{
	public void PlayerDeathHandler()
	{
		EnableCanvas();
	}

	public void PlayerRespawnedHandler()
	{
		DisableCanvas();
	}
}
