﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiManager : MonoBehaviour
{
	#region fields

	[SerializeField] private GameplayCanvas _gameplayCanvas;

	[SerializeField] private StoreCanvas _storeCanvas;

	[SerializeField] private LevelCompleteCanvas _levelCompleteCanvas;

	[SerializeField] private RestartCanvas _restartCanvas;

	[SerializeField] private PlayerStatistics _playerStatistics;

	#endregion

	#region properties

	public static UiManager Instance { get; private set; }

	public GameplayCanvas GameplayCanvas { get { return _gameplayCanvas; } }

	public StoreCanvas StoreCanvas { get { return _storeCanvas; } }

	public LevelCompleteCanvas LevelCompleteCanvas { get { return _levelCompleteCanvas; } }

	public RestartCanvas RestartCanvas { get { return _restartCanvas; } }

	public PlayerStatistics PlayerStatistics { get { return _playerStatistics; } }

	#endregion

	void Awake()
    {
		Instance = this;
		_playerStatistics.NumberOfScoresChangeEvent += _gameplayCanvas.UpdateScoresCountText;

	}

	private void Start()
	{
		_storeCanvas.DisableCanvas();
		_levelCompleteCanvas.DisableCanvas();
		_restartCanvas.DisableCanvas();
		 
		Main.Instance.RestartController.GameRestarted += _restartCanvas.PlayerRespawnedHandler;
		Main.Instance.LevelEnd.LevelFinished += _levelCompleteCanvas.LevelFinishedHandler;
		StoreCanvas.GetCounsButtonClicked += Main.Instance.AdsController.ShowRewardedVideo;
	}

	private void OnDestroy()
	{
		_playerStatistics.NumberOfScoresChangeEvent -= _gameplayCanvas.UpdateScoresCountText;
		Main.Instance.RestartController.GameRestarted -= _restartCanvas.PlayerRespawnedHandler;
		Main.Instance.LevelEnd.LevelFinished -= _levelCompleteCanvas.LevelFinishedHandler;
		StoreCanvas.GetCounsButtonClicked -= Main.Instance.AdsController.ShowRewardedVideo;
	}

}
