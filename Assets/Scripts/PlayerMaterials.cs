﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMaterials : MonoBehaviour
{
	public int _currentSkinIndex;
	[HideInInspector] public Material currentMaterial;
	[SerializeField] private List<Material> _materialsList;


	private void Start()
	{
		Main.Instance.GetPlayer.MyMeshRenderer.material= _materialsList[_currentSkinIndex];
		currentMaterial = _materialsList[_currentSkinIndex];
	}

	public void SetPlayerSkin(int skinIndex)
	{
		Main.Instance.GetPlayer.MyMeshRenderer.material = _materialsList[skinIndex];
		currentMaterial = _materialsList[skinIndex];
		_currentSkinIndex = skinIndex;
		Main.Instance.JsonSaver.SaveGame();
	}
}
