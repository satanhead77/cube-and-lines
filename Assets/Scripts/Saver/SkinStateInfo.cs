﻿
using Newtonsoft.Json;

public struct SkinStateInfo
{
	[JsonProperty] public bool skinIsOpen;

	[JsonConstructor]
	public SkinStateInfo(bool skinIsOpen)
	{
		this.skinIsOpen = skinIsOpen;
	}
}
