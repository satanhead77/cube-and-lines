﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreCanvas : BaseUi
{
	[SerializeField] private List<SkinSelector> _cells;

	[SerializeField] private Transform _layOut;

	[SerializeField] int _numberOfCoinsPerViewedAd;

	public event Action GetCounsButtonClicked;

	private void Start()
	{
		CreatePlayerSelectionCells();
	}

	private void CreatePlayerSelectionCells()
	{
		foreach (var cell in _cells)
		{
			var cellObj = Instantiate(cell);
			cellObj.transform.SetParent(_layOut);
		}
	}

	public void GetCounsButtonClick()
	{
		GetCounsButtonClicked.Invoke();
		Main.Instance.PlayerStatistics.AddScores(_numberOfCoinsPerViewedAd);
	}
}
