﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpeedController : BaseController
{
	public float currentSpeed;
	[SerializeField] private float _maxMoveSpeed;
	[SerializeField] private float _acceleration;

	public override void ControllerUpdate()
	{
		if (_controllerIsEnabled)
			AccelerateSpeed();
	}

	private void AccelerateSpeed()
	{
		if (Main.Instance.Input.Input())
		{
			if (currentSpeed < _maxMoveSpeed)
			{
				currentSpeed += _acceleration * Time.deltaTime;
			}
		}
		else
		{
			currentSpeed = 0;
		}

	}	 
}
