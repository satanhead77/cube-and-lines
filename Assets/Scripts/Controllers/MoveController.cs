﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : BaseController
{
	[SerializeField] private Transform _currentWayPoint;
	[SerializeField] private List<Transform> _wayPointsList;
	[SerializeField] private int wayPointIndex;
	private Vector3 direction;

	public override void ControllerUpdate()
	{
		if(_controllerIsEnabled)
		Move();
	}

	private void Move()
	{
		if (Main.Instance.Input.Input())
		{
			SetCurrentWayPoint();
			direction = _currentWayPoint.position - Position;
			Position = Vector3.MoveTowards(Position, _currentWayPoint.position, Time.deltaTime * Main.Instance.PlayerSpeedController.currentSpeed);
		}
	}

	private void SetCurrentWayPoint()
	{
		if (wayPointIndex <= _wayPointsList.Count - 1)
		{
			_currentWayPoint = _wayPointsList[wayPointIndex];

			float dist = Vector3.Distance(_currentWayPoint.position, Position);

			if (dist < 0.5)
			{
				wayPointIndex++;
			}
		}
	}

	public void RestartWay()
	{
		wayPointIndex = 0;
	}
}
