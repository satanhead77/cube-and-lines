﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinSelector : MonoBehaviour
{
	[SerializeField] private int _skinIndex;

	[SerializeField] private bool _skinIsOpen;

	[SerializeField] private GameObject _buyThisSkinButton;

	[SerializeField] private int _skinPrice;

    [SerializeField] private string _saveFileName; 

	private Button _changeSkinButton;

	void Start()
    {
		 
		_changeSkinButton = GetComponent<Button>();

		_skinIsOpen = Main.Instance.JsonSaver.LoadSkinsState(_saveFileName);
	}

	private void Update()
	{
		if (_skinIsOpen)
		{
			_changeSkinButton.interactable = true;
			_buyThisSkinButton.SetActive(false);
		}
		else
		{
			_changeSkinButton.interactable = false;
			_buyThisSkinButton.SetActive(true);
		}
	}

	public void BuyThisSkinButtonClick()
	{
		if (Main.Instance.PlayerStatistics.RemoveScores(_skinPrice))
		{
			_skinIsOpen = true;

			Main.Instance.JsonSaver.SaveSkinState(_saveFileName, _skinIsOpen);
		}
	}

	public void ChangePlayerSkinButtonClick()
	{
		Main.Instance.PlayerMaterials.SetPlayerSkin(_skinIndex);
	}
}
