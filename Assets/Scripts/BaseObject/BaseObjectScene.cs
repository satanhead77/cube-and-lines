﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseObjectScene : MonoBehaviour
{
	#region Fields

	protected Transform _myTransform;
	protected Vector3 _position;
	protected Quaternion _rotation;
	protected GameObject _instanceObject;
	protected string _name;
	protected MeshRenderer _meshRenderer;

	#endregion

	#region Property
	/// <summary>
	/// Имя объекта
	/// </summary>
	public string Name
	{
		get { return _name; }
		set
		{
			_name = value;
			InstanceObject.name = _name;
		}
	}
	
	/// <summary>
	/// Позиция объекта
	/// </summary>
	public Vector3 Position
	{
		get
		{
			if (InstanceObject != null)
			{
				_position = GetTransform.position;
			}
			return _position;
		}
		set
		{
			_position = value;
			if (InstanceObject != null)
			{
				GetTransform.position = _position;
			}
		}
	}
	
	/// <summary>
	/// Поворот объекта
	/// </summary>
	public Quaternion Rotation
	{
		get
		{
			if (InstanceObject != null)
			{
				_rotation = GetTransform.rotation;
			}

			return _rotation;
		}
		set
		{
			_rotation = value;
			if (InstanceObject != null)
			{
				GetTransform.rotation = _rotation;
			}
		}
	}

	/// <summary>
	/// Ссылка на gameObject
	/// </summary>
	public GameObject InstanceObject
	{
		get { return _instanceObject; }
	}

	/// <summary>
	/// Получить Transform объекта
	/// </summary>
	public Transform GetTransform
	{
		get { return _myTransform; }
	}

	public MeshRenderer MyMeshRenderer
	{
		get { return _meshRenderer; }
		set { _meshRenderer = value; }
	}

	#endregion


	#region UnityFunction

	protected virtual void Awake()
	{
		_instanceObject = gameObject;
		_name = _instanceObject.name;
		_myTransform = _instanceObject.transform;

		if(GetComponent<MeshRenderer>())
		{
			_meshRenderer = GetComponent<MeshRenderer>();
		}
	}

	#endregion

}
