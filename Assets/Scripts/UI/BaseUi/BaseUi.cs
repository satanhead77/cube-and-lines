﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseUi: MonoBehaviour
{
	[SerializeField] protected Canvas _canvas;

	private void Awake()
	{
		_canvas = GetComponent<Canvas>();
	}

	public void EnableCanvas()
	{
		_canvas.enabled = true;
	}

	public void DisableCanvas()
	{
		_canvas.enabled = false;
	}
}
