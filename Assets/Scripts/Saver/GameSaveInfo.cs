﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct GameSaveInfo
{
	[JsonProperty] public int scoresCount;
	[JsonProperty] public int currentPlayerSkin;
	[JsonProperty] public int deathCount;
	[JsonProperty] public int numberOfPassedLevels;

	[JsonConstructor]
	public GameSaveInfo(int scoresCount,int currentPlayerSkin,int deathCount, int numberOfPassedLevels)
	{
		this.scoresCount = scoresCount;
		this.currentPlayerSkin = currentPlayerSkin;
		this.deathCount = deathCount;
		this.numberOfPassedLevels = numberOfPassedLevels;
	}
}

