﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingTrap : BaseTrap
{
	[SerializeField] private float _speed;
	[SerializeField] private float distanceCovered;
	[SerializeField] private bool _isMovingLeft;
	[SerializeField] private float _maxMoveDistanceInSide;
	private Vector3 _startPos;
	private Vector3 _direction;

	private void Start()
	{
		_startPos = Position;
		_direction = Vector3.right;
	}

	public override void TrapMove()
	{
		SetDirection();
		Move();
	}

	private void Move()
	{
		Position += _direction * _speed * Time.deltaTime;

		distanceCovered = Vector3.Distance(_startPos, Position);

		if (distanceCovered > _maxMoveDistanceInSide)
		{
			ChangeBoolValue();
			_startPos = Position;
			distanceCovered = 0;
		}
	}

	private void ChangeBoolValue()
	{
		if (!_isMovingLeft)
		{
			_isMovingLeft = true;
		}
		else
		{
			_isMovingLeft = false;
		}

	}

	private void SetDirection()
	{
		if (!_isMovingLeft)
		{
			_direction = Vector3.right;
		}
		else
		{
			_direction = Vector3.left;
		}
	}
}
