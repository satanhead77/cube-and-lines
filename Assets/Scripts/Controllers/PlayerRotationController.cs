﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotationController : BaseController
{
	[SerializeField] private float _rotateSpeed;
	[SerializeField] private bool _isYRotating;
	[SerializeField] private bool _isXRotating;
	private float _startRotateSpeed;
	private Transform _rotateTarget;

	public override void ControllerUpdate()
	{
		if (_controllerIsEnabled)
		{
			if (Main.Instance.Input.Input())
			{
				RotateY();
				RotateX();
			}
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Turn")
		{
			Debug.Log("Turn");
			_rotateTarget = other.transform;
			_isYRotating = true;
		}

		if (other.gameObject.tag == "VerticalTurn")
		{
			Debug.Log("VerticalTurn");
			_rotateTarget = other.transform;
			_isXRotating = true;
		}
	}

	private void RotateY()
	{
		if (_isYRotating)
		{
			Rotation = Quaternion.RotateTowards(Rotation, _rotateTarget.rotation, _rotateSpeed * Time.deltaTime);

			if (Rotation.y == _rotateTarget.rotation.y)
			{
				_isYRotating = false;
			}
		}
	}


	private void RotateX()
	{
		if (_isXRotating)
		{
			Rotation = Quaternion.RotateTowards(Rotation, _rotateTarget.rotation, _rotateSpeed * Time.deltaTime);

			if (Rotation.x == _rotateTarget.rotation.x)
			{
				_isYRotating = false;
			}
		}
	}
}
